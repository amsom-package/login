# Changelog
<!-- introduction au changelog -->
## V1.2.0 - 04/09/2024

### Fixed

- Correction overlay

## V1.1.0 - 21/05/2024

### Fixed

- Correction overlay

## V1.0.0 - 13/05/2024

### Added

- Ajout des composants (init)

## V0.0.0 - 05/03/2023

### Added

- blabla
- blabla

### Fixed

- blabla
- blabla

### Changed

- blabla
- blabla

### Removed

- blabla
- blabla
