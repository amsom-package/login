# AMSOM Login

Ce package propose un composant AmsomLoginModal et AmsomLoginPage pour afficher le formulaire de login.

## Installation

```bash
npm i @amsom-habitat/login
```
Importer les css dans le main.js tel que :
```javascript
import '@amsom-habitat/ui/dist/style.css'
```

## Développment

Après avoir fait vos dev, veillez à bien tenir à jour le [changelog.md](changelog.md) ainsi que la version du package.json puis faites :
```bash
git add .
git commit -m '<commentaire'
git push origin <branch>
```

## Tests

Les tests sont réalisé de manière automatique sur les branches main et dev mais peuvent être fait localement, notemment pour voir l'evolution du développement via la commande :
```bash
npm run storybook
```

Le valideur devra, si des changements sont observés, aller sur la pipeline pour valider les différences à l'aide de chromatic, sans cela aucun merge-request ne sera possible. Si un merge est effectué, une double verification sera necessaire.

## Déploiement

Après avoir merge les dev sur la branche main, exécutez :
```bash
make publish
```
Cette commande vérifie la version, le changelog et publie le tout

## Utilisation

#### Props

- `loginImage` : Image de la page login
- `error` : Message d'erreur
- `defaultEmailExtension` : Extension par défaut de l'email
- `welcomeMessage` : Message de bienvenue
- `loading` : Boolean pour afficher le loader
- `imageSize` : Taille de l'image (uniquement pour la page)

#### Emits

- `login` : Emet l'événement de login

#### Example complet
```tsx
<tempate>
  <amsom-login-page
      class="vh-100"
      :login-image="loginImg"
      welcome-message="Bienvenue sur Volly"
      :error="error"
      @login="login"
  />
</template>

<script>
import loginImg from '@/assets/images/login.png'; 
import {AmsomLoginPage} from '@amsom-habitat/amsom-login';

  export default {
    name: 'TestPage',
    components: {
      AmsomLoginPage,
    },
    data() {
        return {
          loginImg: loginImg,
          error: '',
        }
    },
    computed: {
    login(){
        //todo
    }
  }
}
</script>
```
