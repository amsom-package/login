import AmsomLoginPage from '../AmsomLoginPage.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
export default {
  title: 'AmsomLoginPage',
  component: AmsomLoginPage,
  tags: ['autodocs'],
  argTypes: {
    //backgroundColor: {
    //  control: 'color',
    //},
    //onClick: {},
    //size: {
    //  control: {
    //    type: 'select',
    //  },
    //  options: ['small', 'medium', 'large'],
    //},
  },
};

export const Default = {
  args: {
    loginImage: "https://picsum.photos/2000/2000",
  },
};
